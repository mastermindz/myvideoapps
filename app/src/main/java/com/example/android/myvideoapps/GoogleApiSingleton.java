package com.example.android.myvideoapps;


import com.google.android.gms.common.api.GoogleApiClient;

public class GoogleApiSingleton {
    private static GoogleApiSingleton singleton = null;
    private GoogleApiClient googleApiClient;


    private GoogleApiSingleton(){ }

    public static GoogleApiSingleton getInstance() {
        if(singleton==null){
            singleton = new GoogleApiSingleton();
        }
        return singleton;
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }
}
